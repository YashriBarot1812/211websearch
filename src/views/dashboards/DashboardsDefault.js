import React, { useState } from 'react';
import { Button, Row, Col, Card, Dropdown, Badge, Modal, Form, ToggleButtonGroup, ToggleButton, Pagination, OverlayTrigger, Tooltip, Popover, Container } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import { OverlayScrollbarsComponent } from 'overlayscrollbars-react';
import HtmlHead from 'components/html-head/HtmlHead';
import BreadcrumbList from 'components/breadcrumb-list/BreadcrumbList';
import CsLineIcons from 'cs-line-icons/CsLineIcons';
import Select from 'react-select';
import ChartCustomHorizontalTooltip from 'views/interface/plugins/chart/ChartCustomHorizontalTooltip';
import ChartSmallLine1 from 'views/interface/plugins/chart/ChartSmallLine1';
import ChartSmallLine2 from 'views/interface/plugins/chart/ChartSmallLine2';
import ChartSmallLine3 from 'views/interface/plugins/chart/ChartSmallLine3';
import ChartSmallLine4 from 'views/interface/plugins/chart/ChartSmallLine4';
import ChartBubble from 'views/interface/plugins/chart/ChartBubble';
import ChartSmallDoughnutChart1 from 'views/interface/plugins/chart/ChartSmallDoughnutChart1';
import ChartSmallDoughnutChart2 from 'views/interface/plugins/chart/ChartSmallDoughnutChart2';
import ChartSmallDoughnutChart3 from 'views/interface/plugins/chart/ChartSmallDoughnutChart3';
import ChartSmallDoughnutChart4 from 'views/interface/plugins/chart/ChartSmallDoughnutChart4';
import ChartSmallDoughnutChart5 from 'views/interface/plugins/chart/ChartSmallDoughnutChart5';
import ChartSmallDoughnutChart6 from 'views/interface/plugins/chart/ChartSmallDoughnutChart6';

import Scrollspy from 'components/scrollspy/Scrollspy';

const DashboardsAnalytic = () => {
  const title = 'Resources';
  const description = 'Resources';

  const breadcrumbs = [
    { to: '', text: 'Home' },
    { to: 'dashboards', text: 'Dashboards' },
    { to: 'interface/components', title: 'Components' },
  ];


  const scrollspyItems = [
    { id: 'title', text: 'Title' },
    { id: 'default', text: 'Default' },
    { id: 'closeButtonOut', text: 'Close Button Out' },
    { id: 'rightModal', text: 'Right Modal' },
    { id: 'leftModal', text: 'Left Modal' },
    { id: 'sizes', text: 'Sizes' },
    { id: 'staticBackdrop', text: 'Static Backdrop' },
    { id: 'overlayScroll', text: 'Overlay Scroll' },
    { id: 'scrollingLongContent', text: 'Scrolling Long Content' },
    { id: 'verticallyCentered', text: 'Vertically Centered' },
    { id: 'tooltipsAndPopovers', text: 'Tooltips and Popovers' },
    { id: 'usingTheGrid', text: 'Using the Grid' },
  ];

  
  const [scrollingModal, setScrollingModal] = useState(false);
  const [scrollingModalBody, setScrollingModalBody] = useState(false);


  function  componentDidMount1(){
    console.log("hello");

    
    fetch('http://211projectappservices-211searchapi.azurewebsites.net/CsvRead?searchText=program', {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
    }).then((response) => response.json())
    .then(data => {
      console.log("responseJson CsvRead");
      console.log(data);
      
      
    })
    .catch(error => {
      console.log('GetTimeSheetReportView error');
      console.log(error);
    });
  }

  componentDidMount1();
  

  return (
    <>
      <HtmlHead title={title} description={description} />
      

      

      

      <Row className="gy-5">
        {/* Consumptions Start */}
        <Col xl="12">
          
          <Card className="card w-100 sh-25 sh-sm-25">
            <img src="/img/banner/cta-wide-2.png" className="card-img h-100" alt="card image" />
            <Row className="gy-5">
            <Col xl="6">
            <div className="card-img-overlay d-flex flex-column justify-content-between bg-transparent">
              
              <div className="mb-3 cta-3 text-primary">Search here!</div>
              <Row className="gx-2">
                <Col xs="12" sm="6">
                  <div className="d-flex flex-column justify-content-start">
                    <div className="text-muted mb-3 mb-sm-0">
                      <input type="email" className="form-control" placeholder="Search" />
                    </div>  
                  </div>
                </Col>
                <Col xs="12" sm="auto">
                  <Button variant="primary" className="btn-icon btn-icon-start">
                    <CsLineIcons icon="search" /> <span>Search</span>
                  </Button>
                </Col>
                <Col xs="12" sm="auto">
                  <Button variant="primary" className="btn-icon btn-icon-start">
                    <CsLineIcons icon="close" /> <span>Clear</span>
                  </Button>
                </Col>

                <Col xs="12" sm="auto">
                  <Button variant="outline-secondary" className="mb-1" onClick={() => setScrollingModal(true)}>
                    Filters
                  </Button>

                  {/* Modal Scrolling long content */}
                  <Modal show={scrollingModal} onHide={() => setScrollingModal(false)} size="lg">
                    <Modal.Header closeButton>
                      <Modal.Title>Filters</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                      <p>
                        Liquorice caramels apple pie chupa chups bonbon. Jelly-o candy apple pie sugar plum icing chocolate cake lollipop jujubes bear claw. Pastry
                        sweet roll carrot cake cake macaroon gingerbread cookie. Lemon drops brownie candy cookie candy pie sweet roll biscuit marzipan. Chocolate
                        bar candy canes macaroon liquorice danish biscuit biscuit. Tiramisu toffee brownie sweet roll sesame snaps halvah. Icing carrot cake cupcake
                        gummi bears danish. Sesame snaps muffin macaroon tiramisu ice cream jelly-o pudding marzipan tootsie roll. Muffin candy icing tootsie roll
                        wafer powder danish cheesecake macaroon. Sweet marshmallow oat cake marshmallow ice cream carrot cake. Bonbon powder carrot cake marzipan
                        jelly beans pie cotton candy cotton candy. Gummies donut caramels chocolate bar. Powder soufflé brownie jelly beans gingerbread candy.
                      </p>
                      <p>
                        Liquorice caramels apple pie chupa chups bonbon. Jelly-o candy apple pie sugar plum icing chocolate cake lollipop jujubes bear claw. Pastry
                        sweet roll carrot cake cake macaroon gingerbread cookie. Lemon drops brownie candy cookie candy pie sweet roll biscuit marzipan. Chocolate
                        bar candy canes macaroon liquorice danish biscuit biscuit. Tiramisu toffee brownie sweet roll sesame snaps halvah. Icing carrot cake cupcake
                        gummi bears danish. Sesame snaps muffin macaroon tiramisu ice cream jelly-o pudding marzipan tootsie roll. Muffin candy icing tootsie roll
                        wafer powder danish cheesecake macaroon. Sweet marshmallow oat cake marshmallow ice cream carrot cake. Bonbon powder carrot cake marzipan
                        jelly beans pie cotton candy cotton candy. Gummies donut caramels chocolate bar. Powder soufflé brownie jelly beans gingerbread candy.
                      </p>

                      <p>
                        Liquorice caramels apple pie chupa chups bonbon. Jelly-o candy apple pie sugar plum icing chocolate cake lollipop jujubes bear claw. Pastry
                        sweet roll carrot cake cake macaroon gingerbread cookie. Lemon drops brownie candy cookie candy pie sweet roll biscuit marzipan. Chocolate
                        bar candy canes macaroon liquorice danish biscuit biscuit. Tiramisu toffee brownie sweet roll sesame snaps halvah. Icing carrot cake cupcake
                        gummi bears danish. Sesame snaps muffin macaroon tiramisu ice cream jelly-o pudding marzipan tootsie roll. Muffin candy icing tootsie roll
                        wafer powder danish cheesecake macaroon. Sweet marshmallow oat cake marshmallow ice cream carrot cake. Bonbon powder carrot cake marzipan
                        jelly beans pie cotton candy cotton candy. Gummies donut caramels chocolate bar. Powder soufflé brownie jelly beans gingerbread candy.
                      </p>

                      <p>
                        Liquorice caramels apple pie chupa chups bonbon. Jelly-o candy apple pie sugar plum icing chocolate cake lollipop jujubes bear claw. Pastry
                        sweet roll carrot cake cake macaroon gingerbread cookie. Lemon drops brownie candy cookie candy pie sweet roll biscuit marzipan. Chocolate
                        bar candy canes macaroon liquorice danish biscuit biscuit. Tiramisu toffee brownie sweet roll sesame snaps halvah. Icing carrot cake cupcake
                        gummi bears danish. Sesame snaps muffin macaroon tiramisu ice cream jelly-o pudding marzipan tootsie roll. Muffin candy icing tootsie roll
                        wafer powder danish cheesecake macaroon. Sweet marshmallow oat cake marshmallow ice cream carrot cake. Bonbon powder carrot cake marzipan
                        jelly beans pie cotton candy cotton candy. Gummies donut caramels chocolate bar. Powder soufflé brownie jelly beans gingerbread candy.
                      </p>

                      <p>
                        Liquorice caramels apple pie chupa chups bonbon. Jelly-o candy apple pie sugar plum icing chocolate cake lollipop jujubes bear claw. Pastry
                        sweet roll carrot cake cake macaroon gingerbread cookie. Lemon drops brownie candy cookie candy pie sweet roll biscuit marzipan. Chocolate
                        bar candy canes macaroon liquorice danish biscuit biscuit. Tiramisu toffee brownie sweet roll sesame snaps halvah. Icing carrot cake cupcake
                        gummi bears danish. Sesame snaps muffin macaroon tiramisu ice cream jelly-o pudding marzipan tootsie roll. Muffin candy icing tootsie roll
                        wafer powder danish cheesecake macaroon. Sweet marshmallow oat cake marshmallow ice cream carrot cake. Bonbon powder carrot cake marzipan
                        jelly beans pie cotton candy cotton candy. Gummies donut caramels chocolate bar. Powder soufflé brownie jelly beans gingerbread candy.
                      </p>
                    </Modal.Body>
                  </Modal>

                </Col>
                <Col xs="12" sm="auto">
                  {/* PK DEV */}
                    
                  {/* PK DEV */}
                </Col>
              </Row>
              <br/>
              <Row className="gx-2">

                <ToggleButtonGroup type="radio" className="d-block" name="buttonOptions2">
                  <ToggleButton id="tbg-radio-3" value={3} variant="outline-primary">
                    Taxonomy
                  </ToggleButton>
                  <ToggleButton id="tbg-radio-4" value={4} variant="outline-primary">
                    Resources
                  </ToggleButton>
                  <ToggleButton id="tbg-radio-5" value={5} variant="outline-primary">
                    Categories
                  </ToggleButton>
                </ToggleButtonGroup>
              </Row>
              
            </div>
            </Col>
            </Row>
          </Card>
        </Col>
        {/* Consumptions End */}

        
      </Row>
      <br/>
      <div className='row'>
        <div className="col-sm-3 col-md-3 col-lg-3 col-xl-3 col-xxl-3">
          <div className='col-sm-12 col-md-12 col-lg-12 col-xl-12 col-xxl-12'>
            <div className='col-md-auto d-none d-lg-block' style={{width:'100%'}} id='scrollSpyMenu'>
              <h2 className='small-title'>
                <b>Saved Searches</b>
              </h2>
              <ul className='nav flex-column'>
                <li>
                  <span>
                    <b>Save the current search:</b>
                  </span>
                  <div className='row col-12'>
                    <div className='col-8'>
                      <input className='form-control' placeholder='Search' />
                    </div>
                    <div className='col-4'>
                      <button type='button' className='btn btn-outline-tertiary mb-4'>Save</button>
                    </div>
                  </div>
                </li>
                <li>
                  <span className='mb-2'>
                    <b>Your Saved Searches:</b>
                  </span>
                </li>
                <li>
                  <div className='row col-12'>
                    <div className='col-8'>
                      <a className='nav-link' href='#imageSquare'>
                        <span>Education</span>
                      </a>
                    </div>
                    <div className='col-4'>
                      <a className='nav-link' href='#imageSquare'>
                        <span>(Remove)</span>
                      </a>
                    </div>
                  </div>
                </li>
                <li>
                  <div className='row col-12'>
                    <div className='col-8'>
                      <a className='nav-link' href='#imageSquare'>
                        <span>Food</span>
                      </a>
                    </div>
                    <div className='col-4'>
                      <a className='nav-link' href='#imageSquare'>
                        <span>(Remove)</span>
                      </a>
                    </div>
                  </div>
                </li>
                <li>
                  <div className='row col-12'>
                    <div className='col-8'>
                      <a className='nav-link' href='#imageSquare'>
                        <span>Housing</span>
                      </a>
                    </div>
                    <div className='col-4'>
                      <a className='nav-link' href='#imageSquare'>
                        <span>(Remove)</span>
                      </a>
                    </div>
                  </div>
                </li>
                <li>
                  <div className='row col-12'>
                    <div className='col-8'>
                      <a className='nav-link' href='#imageSquare'>
                        <span>Paratransit Programs</span>
                      </a>
                    </div>
                    <div className='col-4'>
                      <a className='nav-link' href='#imageSquare'>
                        <span>(Remove)</span>
                      </a>
                    </div>
                  </div>
                </li>
                <li>
                  <div className='row col-12'>
                    <div className='col-8'>
                      <a className='nav-link' href='#imageSquare'>
                        <span>Save 1</span>
                      </a>
                    </div>
                    <div className='col-4'>
                      <a className='nav-link' href='#imageSquare'>
                        <span>(Remove)</span>
                      </a>
                    </div>
                  </div>
                </li>
                <li>
                  <div className='row col-12'>
                    <div className='col-8'>
                      <a className='nav-link' href='#imageSquare'>
                        <span>Sf</span>
                      </a>
                    </div>
                    <div className='col-4'>
                      <a className='nav-link' href='#imageSquare'>
                        <span>(Remove)</span>
                      </a>
                    </div>
                  </div>
                </li>
                <li>
                  <div className='row col-12'>
                    <div className='col-8'>
                      <a className='nav-link' href='#imageSquare'>
                        <span>Utility Assistance</span>
                      </a>
                    </div>
                    <div className='col-4'>
                      <a className='nav-link' href='#imageSquare'>
                        <span>(Remove)</span>
                      </a>
                    </div>
                  </div>
                </li>
                <li>
                  <div className='row col-12'>
                    <div className='col-8'>
                      <a className='nav-link' href='#imageSquare'>
                        <span>water</span>
                      </a>
                    </div>
                    <div className='col-4'>
                      <a className='nav-link' href='#imageSquare'>
                        <span>(Remove)</span>
                      </a>
                    </div>
                  </div>
                </li>
                <li>
                  <div className='row col-12'>
                    <div className='col-8'>
                      <a className='nav-link' href='#imageSquare'>
                        <span>Save 2</span>
                      </a>
                    </div>
                    <div className='col-4'>
                      <a className='nav-link' href='#imageSquare'>
                        <span>(Remove)</span>
                      </a>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </div>
          <br/>
          <div className='col-sm-12 col-md-12 col-lg-12 col-xl-12 col-xxl-12'>
            <div className="col-md-auto d-none d-lg-block" style={{width:'100%'}} id="scrollSpyMenu">
              <h2 className="small-title"><b>Highlighted terms</b></h2>
              <ul className='nav flex-column'>
                <li>
                  <a className='nav-link' href='#title'>
                    <span>Food Banks</span>
                  </a>
                </li>
              </ul>
              <ul className='nav flex-column'>
                <li>
                  <a className='nav-link' href='#title'>
                    <span>Meals</span>
                  </a>
                </li>
              </ul>
              <ul className='nav flex-column'>
                <li>
                  <a className='nav-link' href='#title'>
                    <span>Emergency Shelter</span>
                  </a>
                </li>
              </ul>
              <ul className='nav flex-column'>
                <li>
                  <a className='nav-link' href='#title'>
                    <span>Low Income/Subsidized Rental Housing</span>
                  </a>
                </li>
              </ul>
              <ul className='nav flex-column'>
                <li>
                  <a className='nav-link' href='#title'>
                    <span>Household Goods</span>
                  </a>
                </li>
              </ul>
              <ul className='nav flex-column'>
                <li>
                  <a className='nav-link' href='#title'>
                    <span>Personal Goods/Services </span>
                  </a>
                </li>
              </ul>
              <ul className='nav flex-column'>
                <li>
                  <a className='nav-link' href='#title'>
                    <span>Paratransit Programs</span>
                  </a>
                </li>
              </ul>
              <ul className='nav flex-column'>
                <li>
                  <a className='nav-link' href='#title'>
                    <span>Utility Assistance</span>
                  </a>
                </li>
              </ul>
              <ul className='nav flex-column'>
                <li>
                  <a className='nav-link' href='#title'>
                    <span>Tax Preparation Assistance</span>
                  </a>
                </li>
              </ul>
              <ul className='nav flex-column'>
                <li>
                  <a className='nav-link' href='#title'>
                    <span>Law Enforcement Agencies</span>
                  </a>
                </li>
              </ul>
              <ul className='nav flex-column'>
                <li>
                  <a className='nav-link' href='#title'>
                    <span>Food Banks</span>
                  </a>
                </li>
              </ul>
              <ul className='nav flex-column'>
                <li>
                  <a className='nav-link' href='#title'>
                    <span>Meals</span>
                  </a>
                </li>
              </ul>
              <ul className='nav flex-column'>
                <li>
                  <a className='nav-link' href='#title'>
                    <span>Emergency Shelter</span>
                  </a>
                </li>
              </ul>
              <ul className='nav flex-column'>
                <li>
                  <a className='nav-link' href='#title'>
                    <span>Low Income/Subsidized Rental Housing</span>
                  </a>
                </li>
              </ul>
              <ul className='nav flex-column'>
                <li>
                  <a className='nav-link' href='#title'>
                    <span>Household Goods</span>
                  </a>
                </li>
              </ul>
              <ul className='nav flex-column'>
                <li>
                  <a className='nav-link' href='#title'>
                    <span>Personal Goods/Services </span>
                  </a>
                </li>
              </ul>
              <ul className='nav flex-column'>
                <li>
                  <a className='nav-link' href='#title'>
                    <span>Paratransit Programs</span>
                  </a>
                </li>
              </ul>
              <ul className='nav flex-column'>
                <li>
                  <a className='nav-link' href='#title'>
                    <span>Utility Assistance</span>
                  </a>
                </li>
              </ul>
              <ul className='nav flex-column'>
                <li>
                  <a className='nav-link' href='#title'>
                    <span>Tax Preparation Assistance</span>
                  </a>
                </li>
              </ul>
              <ul className='nav flex-column'>
                <li>
                  <a className='nav-link' href='#title'>
                    <span>Law Enforcement Agencies</span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="col-sm-9 col-md-9 col-lg-9 col-xl-9 col-xxl-9">
          <div className="col-sm-12 col-md-12 col-lg-12 col-xl-12 col-xxl-12">
            <h2 className="small-title"><b>Search Results</b></h2>
            <div className='row row-cols-1 row-cols-lg-1 g-2'>
              <Row className="col-sm-4 col-md-4 col-lg-4 col-xxl-4 text-end mb-1">
                <Col xs="auto" className="mb-3 col-12 me-1">
                  <Select
                    classNamePrefix="react-select"
                    className="sw-18"
                    isSearchable={false}
                    value="10 Per Page"
                    placeholder="5 Per Page"
                  />
                </Col>
              </Row>
              <div className='col-sm-8 col-md-8 col-lg-8 col-xxl-8 mb-1'>
                <nav>
                  <Pagination className="justify-content-end" style={{float:'right'}}>
                    <Pagination.Prev>
                      <CsLineIcons icon="chevron-left" />
                    </Pagination.Prev>
                    <Pagination.Item>1</Pagination.Item>
                    <Pagination.Item active>2</Pagination.Item>
                    <Pagination.Item>3</Pagination.Item>
                    <Pagination.Next>
                      <CsLineIcons icon="chevron-right" />
                    </Pagination.Next>
                  </Pagination>
                </nav>
              </div>
            </div>
            <div className='row row-cols-1 row-cols-lg-1 g-2'>
              <div className='col'>
                <div className='card mb-2 h-100'>
                  <div className="card-body row g-0">
                    <div className='col-auto'>
                      <div className='d-inline-block d-flex'>
                        <div className='bg-gradient-light sw-6 sh-6 rounded-xl'>
                          <div className='text-white d-flex justify-content-center align-items-center h-100'>
                            A
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className='col'>
                      <div className='card-body d-flex flex-column pe-0 pt-0 pb-0 h-100 justify-content-start'>
                        <div className='row align-items-center'>
                          <div className='col-6'>
                            <div className='d-flex align-items-center'>
                              <div className='d-inline-block'>
                                <a href="#">1st Step Program - Early Psychosis Treatment Team</a>
                                <div className='text-muted text-medium'>
                                  <span className='badge bg-outline-tertiary me-1'>
                                    <b>PROGRAM</b>
                                  </span>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className='col-6 text-muted'>
                            <div className='row g-0 justify-content-end'>
                              <div className='col-auto pe-3'>
                                <a href="/dashboards/default" target="_blank">Details</a>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div className='mb-4'>
                          <div className='row g-0 mb-2'>
                            <div className="col lh-1-25 text-alternate">
                              <b>From:</b>
                              Canadian Mental Health Association - Waterloo Wellington Dufferin
                            </div>
                          </div>

                          <div className='row g-0 mb-2'>
                            <div className="col lh-1-25 text-alternate">
                              <b>Application: </b>
                              Self referral * must provide name, address or phone number
                            </div>
                          </div>

                          <div className='row g-0 mb-2'>
                            <div className="col lh-1-25 text-alternate">
                              <b>Eligibility: </b>
                              Oxford County residents experiencing a crisis.
                            </div>
                          </div>

                          <div className='row g-0 mb-2'>
                            <div className="col lh-1-25 text-alternate">
                              <b>Age Requirements:</b>
                              16 year(s) and up.
                            </div>
                          </div>

                          <div className='row g-0 mb-2'>
                            <div className="col lh-1-25 text-alternate">
                              <b>Hours: </b>
                              Mon-Sun 24 hours
                            </div>
                          </div>

                          <div className='row g-0 mb-2'>
                            <div className="col lh-1-25 text-alternate">
                              <b>Fees: </b>
                              None
                            </div>
                          </div>

                          <div className='row g-0 mb-2'>
                            <div className="col lh-1-25 text-alternate">
                              <a href='#'>http://www.oxfordcounty.cmha.ca/</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <br/>
            <div className='row row-cols-1 row-cols-lg-1 g-2'>
              <Row className="col-sm-4 col-md-4 col-lg-4 col-xxl-4 text-end mb-1">
                <Col xs="auto" className="mb-3 col-12 me-1">
                  <Select
                    classNamePrefix="react-select"
                    className="sw-18"
                    isSearchable={false}
                    value="10 Per Page"
                    placeholder="5 Per Page"
                  />
                </Col>
              </Row>
              <div className='col-sm-8 col-md-8 col-lg-8 col-xxl-8 mb-1'>
                <nav>
                  <Pagination className="justify-content-end" style={{float:'right'}}>
                    <Pagination.Prev>
                      <CsLineIcons icon="chevron-left" />
                    </Pagination.Prev>
                    <Pagination.Item>1</Pagination.Item>
                    <Pagination.Item active>2</Pagination.Item>
                    <Pagination.Item>3</Pagination.Item>
                    <Pagination.Next>
                      <CsLineIcons icon="chevron-right" />
                    </Pagination.Next>
                  </Pagination>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </div>

    </>
  );
};

export default DashboardsAnalytic;
